﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Models
{
    public class Horario
    {
        [Key]
        public int id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string HoraIngreso { get; set; }
        public string HoraSalida { get; set; }

    }
}
