﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Models
{
    public class TipoDocIdentidad
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreAbreviado { get; set; }
    }
}
