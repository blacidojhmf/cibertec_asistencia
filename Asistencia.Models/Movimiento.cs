﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Models
{
    public class Movimiento
    {

        public string DocIdentidad { get; set; }
        public string Password { get; set; }
        [Computed]
        public string Nombres { get; set; }
        [Computed]
        public string Fecha { get; set; }
        [Computed]
        public string Hora { get; set; }
        [Computed]
        public string mensaje { get; set; }
    }
}
