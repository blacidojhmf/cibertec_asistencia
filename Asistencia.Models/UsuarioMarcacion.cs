﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Models
{
    public class UsuarioMarcacion
    {
        [Key]
        public int Id { get; set; }
        public int IdTrabajador { get; set; }
        public string CodigoHorario { get; set; }
        public string Codigo { get; set; }
        public string Clave { get; set; }

    }
}
