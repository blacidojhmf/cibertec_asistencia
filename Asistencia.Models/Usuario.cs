﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Asistencia.Models
{
    public class Usuario
    {
        //[Key]
        public int Id { get; set; }
        public int IdTrabajador { get; set; }
        public string Username { get;set; }
        public string Password { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
