﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Models
{
    public class Trabajador
    {
        [Key]
        public int Id { get; set; }
        public string Nombres { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public int IdTipoDocIdentidad { get; set; }
        public string DocIdentidad { get; set; }
        public int IdOficina { get; set; }
        public int IdTipoTrabajador { get; set; }
        public string Sexo { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public string FotoUrl { get; set; }

    }
}
