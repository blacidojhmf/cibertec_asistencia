﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Models
{
    public class Marcacion
    {
        [Key]
        public int Id { get; set; }
        public int IdHorario { get; set; }
        public DateTime FechaEntrada { get; set; }
        public DateTime FechaSalida { get; set; }
        public string Estado { get; set; }
        public int idTrabajador { get; set; }
        
    }

    public class MarcacionTrabajador
    {
        [Key]
        public int Id { get; set; }
        public int IdUsuarioMarcacion { get; set; }
        public int IdHorario { get; set; }
        public DateTime FechaEntrada { get; set; }
        public DateTime FechaSalida { get; set; }
        public string Estado { get; set; }
        public int idTrabajador { get; set; }
        public string nombres { get; set; }
        public string apePaterno { get; set; }
        public string apeMaterno { get; set; }
        public int idOficina { get; set; }
        public int idTipoTrabajador { get; set; }
        public string tipoTrab { get; set; }
        public string nombreAbreviado { get; set; }
        public string oficina { get; set; }
        public string horario { get; set; }
    }

    
}
