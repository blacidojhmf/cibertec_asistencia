﻿using Asistencia.Repositories.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Dapper.Asistencia
{
    public class AsistenciaUnitOfWork: IUnitOfWork
    {
        public AsistenciaUnitOfWork(string connectionString)
        {
            Horario = new HorarioRepository(connectionString);
            Marcacion = new MarcacionRepository(connectionString);
            Oficina = new OficinaRepository(connectionString);
            TipoDocIdentidad = new TipoDocIdentidadRepository(connectionString);
            TipoTrabajador = new TipoTrabajadorRepository(connectionString);
            Trabajador = new TrabajadorRepository(connectionString);
            UsuarioMarcacion = new UsuarioMarcacionRepository(connectionString);
            Usuario = new UsuarioRepository(connectionString);
            Movimiento = new MovimientoRepository(connectionString);
        }

        public IHorarioRepository Horario { get; private set; }
        public IMarcacionRepository Marcacion { get; private set; }
        public IOficinaRepository Oficina { get; private set; }
        public ITipoDocIdentidadRepository TipoDocIdentidad { get; private set; }
        public ITipoTrabajadorRepository TipoTrabajador { get; private set; }
        public ITrabajadorRepository Trabajador { get; private set; }
        public IUsuarioMarcacionRepository UsuarioMarcacion { get; private set; }
        public IUsuarioRepository Usuario { get; private set; }
            public IMovimientoRepository Movimiento { get; private set; }
    }
}
