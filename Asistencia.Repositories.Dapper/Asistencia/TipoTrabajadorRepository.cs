﻿using Asistencia.Models;
using Asistencia.Repositories.Asistencia;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Dapper.Asistencia
{
    public class TipoTrabajadorRepository: Repository<TipoTrabajador>, ITipoTrabajadorRepository
    {
        public TipoTrabajadorRepository(string connectionString) : base(connectionString)
        {

        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("Delete from TipoTrabajador where id = @id", new { id = id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
