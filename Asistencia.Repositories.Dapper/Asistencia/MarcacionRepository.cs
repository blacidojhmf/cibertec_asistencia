﻿using Asistencia.Models;
using Asistencia.Repositories.Asistencia;
using Dapper;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Dapper.Asistencia
{
    public class MarcacionRepository:Repository<Marcacion>, IMarcacionRepository
    {
        public MarcacionRepository(string connectionString) : base(connectionString)
        {

        }

        public Marcacion GetById(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.GetAll<Marcacion>().Where(
                    marcacion => marcacion.Id.Equals(id)).First();
            }
        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("Update Marcacion set RegEstado = 0 where id = @id", new { id = id });
                return Convert.ToBoolean(result);
            }
        }
       
        public IEnumerable<MarcacionTrabajador> PagedList(int startRow, int endRow)
        {
            if (startRow >= endRow) return new List<MarcacionTrabajador>();
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@startRow", startRow);
                parameters.Add("@endRow", endRow);
                return connection.Query<MarcacionTrabajador>("dbo.uspMarcacionPagedList",
                    parameters,
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }
        public int Count()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.Marcacion");
            }
        }

        public bool Update(Marcacion marcacion)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("update Marcacion " +
                                                "set IdHorario = @horario, " +
                                                "FechaEntrada = @fechaEntrada, " +
                                                "FechaSalida = @fechaSalida, " +
                                                "Estado = @estado, " +
                                                "IdTrabajador = @idTrabajador, " +
                                                "where Id = @myId",
                                                new
                                                {
                                                    horario = marcacion.IdHorario,
                                                    fechaEntrada = marcacion.FechaEntrada,
                                                    fechaSalida = marcacion.FechaSalida,
                                                    estado = marcacion.Estado,
                                                    idTrabajador = marcacion.idTrabajador,
                                                    myId = marcacion.Id
                                                });
                return Convert.ToBoolean(result);
            }
        }

        public bool Insert(MarcacionTrabajador marcacion)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("Insert Marcacion (IdHorario,IdTrabajador,FechaEntrada,FechaSalida,Estado) Values  (" +
                                                "@horario, " +
                                                "@idTrabajador " +
                                                "@fechaEntrada, " +
                                                "@fechaSalida, " +
                                                "@estado " ,
                                                new
                                                {
                                                    horario = marcacion.IdHorario,
                                                    idTrabajador = marcacion.idTrabajador,
                                                    fechaEntrada = marcacion.FechaEntrada,
                                                    fechaSalida = marcacion.FechaSalida,
                                                    estado = marcacion.Estado
                                                });
                return Convert.ToBoolean(result);
            }
        }

    }
   
}
