﻿using Asistencia.Models;
using Asistencia.Repositories.Asistencia;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Dapper.Asistencia
{
    public class MovimientoRepository : Repository<Movimiento>, IMovimientoRepository
    {
        public MovimientoRepository(string connectionString) : base(connectionString) { }

        public Movimiento CrearMovimiento(string docIdentidad, string password)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@docIdentidad", docIdentidad);
                parameters.Add("@password", password);

                return connection.QueryFirstOrDefault<Movimiento>("dbo.UspCrearMovimiento", parameters,
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }


        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                return false;
            }
        }
    }
}
