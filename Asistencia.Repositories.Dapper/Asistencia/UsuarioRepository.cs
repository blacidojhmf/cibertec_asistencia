﻿using Asistencia.Models;
using Asistencia.Repositories.Asistencia;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Dapper.Asistencia
{
    public class UsuarioRepository:Repository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(string connectionString) : base(connectionString)
        { 
        
        }
        public Usuario ValidarUsuario(string username, string password, string email)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("@username", username);
                parameters.Add("@password", password);

                return connection.QueryFirstOrDefault<Usuario>("dbo.UspValidarUsuario", parameters,
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public Usuario CrearUsuario(Usuario usuario)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                String message = "";
                var parameters = new DynamicParameters();
                parameters.Add("@idTrabajador", usuario.IdTrabajador);
                parameters.Add("@username", usuario.Username);
                parameters.Add("@password", usuario.Password);
                parameters.Add("@ov_message_error", message);

                return connection.QueryFirstOrDefault<Usuario>("dbo.UspCrearUsuario", parameters,
                    commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public bool Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var result = connection.Execute("Update Usuario set RegEstado = 0 where id = @id", new { id = id });
                return Convert.ToBoolean(result);
            }
        }
    }
}
