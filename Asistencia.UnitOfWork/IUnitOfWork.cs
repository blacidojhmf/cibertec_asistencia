﻿using Asistencia.Repositories.Asistencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.UnitOfWork
{
    public interface IUnitOfWork
    {
        IHorarioRepository Horario { get; }
        IMarcacionRepository Marcacion { get; }
        IOficinaRepository Oficina { get; }
        ITipoDocIdentidadRepository TipoDocIdentidad { get; }
        ITipoTrabajadorRepository TipoTrabajador { get; }
        ITrabajadorRepository Trabajador { get; }
        IUsuarioMarcacionRepository UsuarioMarcacion { get; }
        IUsuarioRepository Usuario { get; }
        IMovimientoRepository Movimiento { get; }
    }
}
