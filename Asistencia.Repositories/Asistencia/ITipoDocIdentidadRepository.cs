﻿using Asistencia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Asistencia
{
    public interface ITipoDocIdentidadRepository: IRepository<TipoDocIdentidad>
    {
        bool Delete(int id);
    }
}
