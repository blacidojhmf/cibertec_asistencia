﻿using Asistencia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Asistencia
{
    public interface IMarcacionRepository:IRepository<Marcacion>
    {
        bool Delete(int id);
        IEnumerable<MarcacionTrabajador> PagedList(int startRow, int endRow);
        int Count();
        bool Insert(MarcacionTrabajador marcacion);


    }
}
