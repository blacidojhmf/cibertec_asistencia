﻿using Asistencia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asistencia.Repositories.Asistencia
{
    public interface IUsuarioRepository:IRepository<Usuario>
    {
        Usuario ValidarUsuario(string username, string password, string email);
        Usuario CrearUsuario(Usuario usuario);

        bool Delete(int id);

    }
}
