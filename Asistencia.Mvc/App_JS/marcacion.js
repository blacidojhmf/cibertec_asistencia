﻿(function (marcacion) {
    marcacion.success = successReload;
    marcacion.pages = 1;
    marcacion.rowSize = 10;
    marcacion.hub = {};
    marcacion.ids = [];
    marcacion.recordInUse = false;

    marcacion.addMarcacion = addMarcacionId;
    marcacion.removeMarcacion = removeMarcacionId;
    marcacion.validate = validate;

    $(function () {
        connectToHub();
        init();
    });

    return marcacion;

    function successReload(option) {
        cibertec.closeModal(option);
        //alert("Todo bien");
    }

    function init() {
        $.get('/Marcacion/CountPages/' + marcacion.rowSize,
            function (data) {
                marcacion.pages = data;
                $('.pagination').bootpag({
                    total: marcacion.pages,
                    page: 1,
                    maxVisible: 5,
                    leaps: true,
                    firstLastUse: true,
                    first: '<-',
                    last: '->',
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                }).on('page', function (event, num) {
                    getMarcacion(num);
                });
                getMarcacion(1);
            });
    }
    function getMarcacion(num) {
        var url = '/Marcacion/List/' + num + '/' + marcacion.rowSize;
        $.get(url, function (data) {
            $('.content').html(data);
        })
    }

    function addMarcacionId(id) {
        marcacion.hub.server.addMarcacionId(id);
    }
    function removeMarcacionId(id) {
        marcacion.hub.server.removeMarcacionId(id);
    }
    function connectToHub() {
        marcacion.hub = $.connection.marcacionHub;
        marcacion.hub.client.marcacionStatus = marcacionStatus;
    }
    function marcacionStatus(marcacionIds) {
        console.log(marcacionIds);
        marcacion.ids = marcacionIds;
    }
    function validate(id) {
        marcacion.recordInUse = (marcacion.ids.indexOf(id) > -1);
        
        if (marcacion.recordInUse) {
            $('#inUse').removeClass('hidden');
            $('#btn-save').html("");
        }
    }
})(window.marcacion = window.marcacion || {});