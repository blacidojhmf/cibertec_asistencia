﻿(function (trabajador) {
    trabajador.success = successReload;
    trabajador.pages = 1;
    trabajador.rowSize = 5;
    //trabajador.hub = {};
    trabajador.ids = [];
    trabajador.recordInUse = false;

    trabajador.addTrabajador = addTrabajador;
    trabajador.removeTrabajador = removeTrabajadorId;
    //trabajador.validate = validate;

    //trabajador.searchByFilter = searchByFilter;


    $(function () {
        connectToHub();
        init();
    });
    return trabajador;

    function successReload(option) {
        //alert("Todo bien");
        cibertec.closeModal(option);

        //Uso del DOM: Document Object Model
        elements = document.getElementsByClassName('active');  //elementos <li>
        activePage = elements[0].children;                     //elemento <a>
        getTrabajador(activePage[0].text);                      //texto de la etiqueta <a>

        console.log("Elementos:");
        console.log(elements);
        console.log("Hijo del elemento [0]:");
        console.log(activePage);
        console.log("número de página activa: " + activePage[0].text);
    }

    function init() {
        $.get('/trabajador/CountPages/' + trabajador.rowSize,
            function (data) {
                trabajador.pages = data;
                $('.pagination').bootpag({
                    total: customer.pages,
                    page: 1,
                    maxVisible: 5,
                    leaps: true,
                    firstLastUse: true,
                    first: '<-',
                    last: '->',
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                }).on('page', function (event, num) {
                    gettrabajador(num);
                });
                gettrabajador(1);
            });
    }
    function gettrabajador(num) {
        var url = '/Trabajador/List/' + num + '/' + trabajador.rowSize;
        $.get(url, function (data) {
            $('.content').html(data);
        })
    }

    /*function searchByFilter() {
        var trabajadorId = document.getElementById("id");
        var trabajadorName = document.getElementById("trabajadorName");
        console.log(trabajadorId.value + "---" + trabajadorName.value);
        if (trabajadorId.value == '') trabajadorId.value = '-'
        if (trabajadorName.value == '') trabajadorName.value = '-'
        var url = '/Customer/ListByFilters/' + trabajadorId.value + '/' + trabajadorName.value;
        $.get(url, function (data) {
            $('.content').html(data);
        })

    }*/
    /*Funciones del Signal-R*/
    function addTrabajador(id) {
        trabajador.hub.server.addTrabajadorId(id);
    }
    function removeTrabajadorId(id) {
        trabajador.hub.server.removeTrabajadorId(id);
    }
    function connectToHub() {
        trabajador.hub = $.connection.trabajadorHub;
        trabajador.hub.client.trabajadorStatus = customerStatus;
    }
    /*function customerStatus(customerIds) {
        console.log(customerIds);
        trabajador.ids = customerIds;
    }
    function validate(id) {
        trabajador.recordInUse = (customer.ids.indexOf(id) > -1);
        if (customer.recordInUse) {
            $('#inUse').removeClass('hidden');
            $('#btn-save').html("");
        }
    }*/

})(window.trabajador = window.trabajador || {});