﻿(function (cibertec) {
    cibertec.getModal = getModalContent;
    cibertec.closeModal = closeModal;
    return cibertec;

    function getModalContent(url) {
        $.get(url, function (data) {
            $('.modal-body').html(data);
        })
    }

    function closeModal(option) {
        $("button[data-dismiss='modal']").click();
        $('.modal-body').html("");
        modifyAlertsClasses(option);
    }

    function modifyAlertsClasses(option) {
        $('#createMessage').addClass('hidden');
        $('#deleteMessage').addClass('hidden');
        $('#editMessage').addClass('hidden');

        if (option === 'create') {
            $('#createMessage').removeClass('hidden');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Creación exitosa!',
                showConfirmButton: false,
                timer: 4000
            });
        }
        else if (option === 'edit') {
            //alert("Se editó exitosamente");
            $('#editMessage').removeClass('hidden');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Edición exitosa!',
                showConfirmButton: false,
                timer: 4000
            });
        }
        else if (option === 'delete') {
            $('#deleteMessage').removeClass('hidden');
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Eliminación exitosa!',
                showConfirmButton: false,
                timer: 4000
            });
        }

        window.setTimeout(function () {
            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 5000);
    }
})(window.cibertec = window.cibertec || {});