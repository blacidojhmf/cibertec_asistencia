﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//Agregamos el ensamblado : clase de Owin
[assembly: OwinStartup(typeof(Asistencia.Mvc.Startup))]
namespace Asistencia.Mvc
{
    
    public class Startup
    {
        public void Configuration(IAppBuilder app) // Parametro: El objeto de la aplicacion
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Account/Login")
            });
            app.MapSignalR();
        }
    }
}