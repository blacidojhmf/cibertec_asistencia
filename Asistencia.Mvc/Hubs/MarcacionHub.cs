﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Asistencia.Mvc.Hubs
{
    public class MarcacionHub: Hub
    {
        static List<string> MarcacionIds = new List<string>();

        public void AddMarcacionId(string id)
        {
            if (!MarcacionIds.Contains(id))
            {

                MarcacionIds.Add(id);
            }
            Clients.All.customerStatus(MarcacionIds);
        }
        public void RemoveMarcacionId(string id)
        {
            if (MarcacionIds.Contains(id)) MarcacionIds.Remove(id);
            Clients.All.marcacionStatus(MarcacionIds);
        }
        public override Task OnConnected()
        {
            return Clients.All.marcacionStatus(MarcacionIds);
        }
        public void Message(string message)
        {
            Clients.All.getMessage(message);
        }
        
        

    }
}