﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Asistencia.Mvc.Models
{
    public class MovimientoViewModel
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "D.N.I.")]
        public string DocIdentidad { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
    }
}