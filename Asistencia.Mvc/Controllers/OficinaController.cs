﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class OficinaController : BaseController
    {
        public OficinaController(IUnitOfWork unit): base(unit)
        {
 
        }
        // GET: Oficina
        public ActionResult Index()
        {
            return View(_unit.Oficina.GetList());
        }

        public ActionResult Create()
        {
            return View(new Oficina());
        }

        [HttpPost]
        public ActionResult Create(Oficina oficina)
        {
            if (ModelState.IsValid)
            {
                _unit.Oficina.Insert(oficina);
                return RedirectToAction("Index");
            }
            return View(oficina);
        }

        public ActionResult Edit(int id)
        {
            return View(_unit.Oficina.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Oficina oficina)
        {
            if (_unit.Oficina.Update(oficina))
            {
                return RedirectToAction("Index");
            }
            return View(oficina);
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.Oficina.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Oficina.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.Oficina.GetById(id));
        }
    }
}