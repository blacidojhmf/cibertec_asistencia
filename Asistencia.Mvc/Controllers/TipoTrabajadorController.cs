﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class TipoTrabajadorController : BaseController
    {
        public TipoTrabajadorController(IUnitOfWork unit): base(unit)
        {

        }
        // GET: TipoTrabajador
        public ActionResult Index()
        {
            return View(_unit.TipoTrabajador.GetList());
        }

        public ActionResult Create()
        {
            return View(new TipoTrabajador());
        }

        [HttpPost]
        public ActionResult Create(TipoTrabajador tipoTrabajador)
        {
            if (ModelState.IsValid)
            {
                _unit.TipoTrabajador.Insert(tipoTrabajador);
                return RedirectToAction("Index");
            }
            return View(tipoTrabajador);
        }

        public ActionResult Edit(int id)
        {
            return View(_unit.TipoTrabajador.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(TipoTrabajador tipoTrabajador)
        {
            if (_unit.TipoTrabajador.Update(tipoTrabajador))
            {
                return RedirectToAction("Index");
            }
            return View(tipoTrabajador);
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.TipoTrabajador.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.TipoTrabajador.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.TipoTrabajador.GetById(id));
        }
    }
}