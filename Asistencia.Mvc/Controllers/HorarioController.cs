﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class HorarioController : BaseController
    {
        public HorarioController(IUnitOfWork unit) : base(unit)
        {

        }
        // GET: Horario
        public ActionResult Index()
        {
            return View(_unit.Horario.GetList());
        }

        public ActionResult Create()
        {
            return View(new Horario());
        }

        [HttpPost]
        public ActionResult Create(Horario horario)
        {
            if (ModelState.IsValid)
            {
                _unit.Horario.Insert(horario);
                return RedirectToAction("Index");
            }
            return View(horario);
        }

        public ActionResult Edit(int id)
        {
            return View(_unit.Horario.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Horario horario)
        {
            if (_unit.Horario.Update(horario))
            {
                return RedirectToAction("Index");
            }
            return View(horario);
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.Horario.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Horario.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.Horario.GetById(id));
        }
    }
}