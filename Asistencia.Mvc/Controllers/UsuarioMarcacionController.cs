﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class UsuarioMarcacionController : BaseController
    {
        public UsuarioMarcacionController(IUnitOfWork unit): base(unit)
        {

        }
        // GET: UsuarioMarcacion
        public ActionResult Index()
        {
            return View(_unit.UsuarioMarcacion.GetList());
        }

        public ActionResult Create()
        {
            return View(new UsuarioMarcacion());
        }

        [HttpPost]
        public ActionResult Create(UsuarioMarcacion usuarioMarcacion)
        {
            if (ModelState.IsValid)
            {
                _unit.UsuarioMarcacion.Insert(usuarioMarcacion);
                return RedirectToAction("Index");
            }
            return View(usuarioMarcacion);
        }

        public ActionResult Edit(int id)
        {
            return View(_unit.UsuarioMarcacion.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(UsuarioMarcacion usuarioMarcacion)
        {
            if (_unit.UsuarioMarcacion.Update(usuarioMarcacion))
            {
                return RedirectToAction("Index");
            }
            return View(usuarioMarcacion);
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.UsuarioMarcacion.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.UsuarioMarcacion.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.UsuarioMarcacion.GetById(id));
        }
    }
}