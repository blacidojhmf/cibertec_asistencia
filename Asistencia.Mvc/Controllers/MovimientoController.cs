﻿using Asistencia.Models;
using Asistencia.Mvc.Models;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class MovimientoController : BaseController
    {
        public MovimientoController(IUnitOfWork unit) : base(unit) { }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new MovimientoViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(MovimientoViewModel userView)
        {
            if (!ModelState.IsValid) return View(userView);

            var validUser = _unit.Movimiento.CrearMovimiento(userView.DocIdentidad, userView.Password);
            if (validUser == null)
            {
                ModelState.AddModelError("Error", "Invalid values");
                return View(userView);
            }

            return RedirectToAction("Register", "Movimiento");
        }
    }
}