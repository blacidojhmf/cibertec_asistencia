﻿using Asistencia.Models;
using Asistencia.Mvc.Models;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Account
        public AccountController(IUnitOfWork unit) : base(unit) {  }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View(new UsuarioViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(UsuarioViewModel usuario)
        {
            if (!ModelState.IsValid) return View(usuario);

            var validUser = _unit.Usuario.ValidarUsuario(usuario.Username, usuario.Password, usuario.Email);
            if (validUser == null)
            {
                ModelState.AddModelError("Error", "Usuario o contraseña es invalido");
                return View(usuario);
            }

            var identity = new ClaimsIdentity(new[]
            {
                //new Claim(ClaimTypes.Email, validUser.Email),
                new Claim(ClaimTypes.Name, validUser.Username),
                new Claim(ClaimTypes.NameIdentifier, validUser.Username)
            }, "ApplicationCookie");

            //Contexto de la ejecucion de la aplicacion
            var context = Request.GetOwinContext();

            var authManager = context.Authentication;

            //Indicamos la entidad logeada
            authManager.SignIn(identity);

            return RedirectToLocal(usuario.ReturnUrl);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View(new RegistrarUsuarioViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegistrarUsuarioViewModel userView)
        {
            if (!ModelState.IsValid) return View(userView);

            Usuario user = new Usuario
            {
                Username = userView.Username,
                Password = userView.Password,
                IdTrabajador = userView.IdTrabajador
            };
            var validUser = _unit.Usuario.CrearUsuario(user);
            if (validUser == null)
            {
                ModelState.AddModelError("Error", "Invalid email or password");
                return View(userView);
            }

            return RedirectToAction("Login", "Account");
        }

        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var authManager = context.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Login", "Account");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }


    }
}