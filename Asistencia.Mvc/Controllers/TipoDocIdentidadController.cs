﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class TipoDocIdentidadController : BaseController
    {
        public TipoDocIdentidadController(IUnitOfWork unit): base(unit)
        {

        }
        // GET: TipoDocIdentidad
        public ActionResult Index()
        {
            return View(_unit.TipoDocIdentidad.GetList());
        }

        public ActionResult Create()
        {
            return View(new TipoDocIdentidad());
        }

        [HttpPost]
        public ActionResult Create(TipoDocIdentidad tipoDocIdentidad)
        {
            if (ModelState.IsValid)
            {
                _unit.TipoDocIdentidad.Insert(tipoDocIdentidad);
                return RedirectToAction("Index");
            }
            return View(tipoDocIdentidad);
        }

        public ActionResult Edit(int id)
        {
            return View(_unit.TipoDocIdentidad.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(TipoDocIdentidad tipoDocIdentidad)
        {
            if (_unit.TipoDocIdentidad.Update(tipoDocIdentidad))
            {
                return RedirectToAction("Index");
            }
            return View(tipoDocIdentidad);
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.TipoDocIdentidad.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.TipoDocIdentidad.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.TipoDocIdentidad.GetById(id));
        }
    }
}