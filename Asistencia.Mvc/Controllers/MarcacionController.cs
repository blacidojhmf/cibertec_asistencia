﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    [RoutePrefix("Marcacion")]
    public class MarcacionController : BaseController
    {
        public MarcacionController(IUnitOfWork unit):base(unit)
        {

        }
        // GET: Marcacion
        public ActionResult Index()
        {
            return View(_unit.Marcacion.GetList());
        }

        public PartialViewResult Create()
        {
            return PartialView("_Create", new Marcacion());
        }
        [HttpPost]
        public ActionResult Create(MarcacionTrabajador marcacion)
        {
            if (ModelState.IsValid)
            {
                _unit.Marcacion.Insert(marcacion);
                return RedirectToAction("Index");
            }
          
            return PartialView("_Create", marcacion);
        }

        public PartialViewResult Edit(String id)
        {
            int Id = Convert.ToInt32(id);
            var personal = _unit.Trabajador.GetList();
            ViewBag.trabajadores = personal;
            return PartialView("_Edit", _unit.Marcacion.GetById(Id));
        }
        [HttpPost]
        public ActionResult Edit(Marcacion marcacion)
        {
            if (_unit.Marcacion.Update(marcacion)) return RedirectToAction("Index");
            return PartialView("_Edit", marcacion);
        }

        public PartialViewResult Delete(String id)
        {
            int Id = Convert.ToInt32(id);
            return PartialView("_Delete", _unit.Marcacion.GetById(Id));
        }
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(String id)
        {
            int Id = Convert.ToInt32(id);
            if (_unit.Marcacion.Delete(_unit.Marcacion.GetById(Id))) return RedirectToAction("Index");
            
            return PartialView("_Delete", _unit.Marcacion.GetById(Id));
        }

        public PartialViewResult Details(String id)
        {

            return PartialView("_Details", _unit.Marcacion.GetById(id));
        }

        [Route("CountPages/{rowSize:int}")]
        public int CountPages(int rowSize)
        {
            var totalRecords = _unit.Marcacion.Count();
            return totalRecords % rowSize != 0 ? (totalRecords / rowSize) + 1 : totalRecords / rowSize;
        }
        [Route("List/{page:int}/{rows:int}")]
        public PartialViewResult List(int page, int rows)
        {
            if (page <= 0 || rows <= 0) return PartialView(new List<Marcacion>());
            var startRecord = ((page - 1) * rows) + 1;
            var endRecord = page * rows;
            return PartialView("_List", _unit.Marcacion.PagedList(startRecord, endRecord));
        }
        /*
        [Route ("ListByFilter/{Id:int}/{trabajadorName:string}")]
        public async Task<>*/
    }
}