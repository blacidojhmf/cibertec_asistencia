﻿using Asistencia.Models;
using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Asistencia.Mvc.Controllers
{
    public class TrabajadorController : BaseController
    {
        public TrabajadorController(IUnitOfWork unit): base(unit)
        {

        }
        // GET: Trabajador
        public ActionResult Index()
        {
            return View(_unit.Trabajador.GetList());
        }

        public ActionResult Create()
        {
            return View(new Trabajador());
        }

        [HttpPost]
        public ActionResult Create(Trabajador trabajador)
        {
            if (ModelState.IsValid)
            {
                _unit.Trabajador.Insert(trabajador);
                return RedirectToAction("Index");
            }
            return View(trabajador);
        }

        public ActionResult Edit(int id)
        {
            return View(_unit.Trabajador.GetById(id));
        }

        [HttpPost]
        public ActionResult Edit(Trabajador trabajador)
        {
            if (_unit.Trabajador.Update(trabajador))
            {
                return RedirectToAction("Index");
            }
            return View(trabajador);
        }

        public ActionResult Delete(int id)
        {
            return View(_unit.Trabajador.GetById(id));
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            if (_unit.Trabajador.Delete(id))
            {
                return RedirectToAction("Index");
            }
            return View(_unit.Trabajador.GetById(id));
        }
    }
}