﻿using Asistencia.Models;
using Asistencia.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asistencia.WebApi.Controller
{
    public class TrabajadorController : BaseController
    {
        public TrabajadorController(IUnitOfWork unit): base(unit)
        {

        }

        [Route("{id}")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            if (id == "" || id == null) return BadRequest();
            return Ok(_unit.Trabajador.GetById(Int32.Parse(id)));
        }

        [Route("lista")]
        [HttpGet]
        public IHttpActionResult GetList()
        {
            return Ok(_unit.Trabajador.GetList());
        }

        //[Route("CountPages/{rowSize:int}")]
        /*public int CountPages(int rowSize)
        {
            //var totalRecords = _unit.Trabajador.Count();
            //return totalRecords % rowSize != 0 ? (totalRecords / rowSize) + 1 : totalRecords / rowSize;
        }*/

        //[Route("pagina/{page:int}/{rows:int}")]
        //[HttpGet]
        /*public IHttpActionResult GetList(int page, int rows)
        {
            if (page <= 0 || rows <= 0) return Ok(new List<Trabajador>());
            var startRecord = ((page - 1) * rows) + 1;
            var endRecord = page * rows;
            //return Ok(_unit.Trabajador.PagedList(startRecord, endRecord));
        }*/
    }
}