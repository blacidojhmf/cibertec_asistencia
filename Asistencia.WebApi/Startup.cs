﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Asistencia.WebApi.App_Start;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Asistencia.WebApi.Startup))]

namespace Asistencia.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            DIConfig.ConfigureInjector(config);
            RouteConfig.Register(config);
            app.UseWebApi(config);
        }
    }
}
