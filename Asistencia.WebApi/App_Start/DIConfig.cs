﻿using Asistencia.Repositories.Dapper.Asistencia;
using Asistencia.UnitOfWork;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Configuration;
//using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Asistencia.WebApi.App_Start
{
    public class DIConfig
    {
        public static void ConfigureInjector(HttpConfiguration config)
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            container.Register<IUnitOfWork>(() =>
               new AsistenciaUnitOfWork(ConfigurationManager.ConnectionStrings["AsistenciaConnection"].ToString()));
            container.RegisterWebApiControllers(config);
            container.Verify();
            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}